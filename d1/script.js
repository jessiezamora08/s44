// get post data
//promise - holds the eventual result of an asynchronous operation
// promise states - pending, resolved or fulfilled, rejected
//Asynchrounous Operation - any js operation that would take time to complete like networks request

// How to consume promises
fetch("https://jsonplaceholder.typicode.com/posts") // returns only a promise
  .then((response) => response.json())
  .then((data) => showPosts(data))
  .catch((err) => console.log(err));

const showPosts = (posts) => {
  let postEntries = "";
  posts.forEach((post) => {
    postEntries +=
      // +=(current value then add)
      `<div id = "post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3> 
            <p id= "post-body-${post.id}">${post.body}</p>

            <button onclick = "editPost(${post.id})">Edit</button>
            <button onclick="deletePost(${post.id})">Delete</button>
        </div>
        
        `;
  });
  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//DELETE POST


const deletePost = (id) => {
  //alert('Delete this post' + id)
  fetch(`https://jsonplaceholder.typicode.com/posts"/${id}`)
}

//edit post 


const editPost = (id) => {
  //alert ('edit this post' + id)  

  let title = document.querySelector(`#post-title-${id}`).innerHTML
  let body = document.querySelector(`#post-body-${id}`).innerHTML

  document.getElementById("txt-edit-id").value = id
  document.getElementById("txt-edit-title").value = title
  document.getElementById("txt-edit-body").value = body 
  document.querySelector("#btn-submit-update").removeAttribute('disabled')

  //Update 

  let updateForm = document.querySelector('#form-edit-post')
  updateForm.addEventListener('submit', (e) => {
    e.preventDefault()

    //console.log(id)
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
      method: 'PUT',
      body: JSON.stringify({
          id: id,
          title: document.querySelector('#txt-edit-title').value,
          body:  document.querySelector('#txt-edit-body').value,
          userId: 1
      }),
      headers: {'content-type':'application/json; charset=UTF-8'}
    })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert('Post Updated')
    })

  })
}

//ADD POST

let addPostForm = document.querySelector("#form-add-post");
addPostForm.addEventListener("submit", (e) => {
  e.preventDefault();

  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "post",
    body: JSON.stringify({
      title: document.getElementById("txt-title").value,
      body: document.getElementById("txt-body").value,
      userId: 1,
    }),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Post Added");
      document.querySelector('#txt-title').value='' // to clear out once nakapag dagdag na ng input 
      document.querySelector('#txt-body').value=''
    });
});



//const p = new Promise((resolve, reject) => {
//async operation

// resolve(1)

/*
    output of res(1)
    Promise {<fulfilled>: 1}
    [[Prototype]]: Promise
    [[PromiseState]]: "fulfilled"
    [[PromiseResult]]: 1
*/

// reject(new Error('message'))

/*
output of rej:
Promise {<rejected>: Error: message
    at file:///C:/Users/Jessie/Documents/b-146/s44/d1/script.js:20:5
    at new Prom…}
 */
//});

//console.log(p);
// p.then(result => console.log ('Result:',result))
// .catch(err => console.log('Error:',err.message))
